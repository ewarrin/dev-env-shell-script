#!/bin/bash
# Eric's Development Machine Setup on Ubuntu
# Author : Eric Arrington

# Set the colours you can use
black=$(tput setaf 0)
red=$(tput setaf 1)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
blue=$(tput setaf 4)
magenta=$(tput setaf 5)
cyan=$(tput setaf 6)
white=$(tput setaf 7)

# Set continue to false by default.
CONTINUE=false

# Resets the style
reset='tput sgr0'

echo "" $blue
echo "###############################################" $blue
echo "#        DO NOT RUN THIS SCRIPT BLINDLY       #" $blue
echo "#         YOU'LL PROBABLY REGRET IT...        #" $blue
echo "#                                             #" $blue
echo "#              READ IT THOROUGHLY             #" $blue
echo "#         AND EDIT TO SUIT YOUR NEEDS         #" $blue
echo "###############################################" $blie
echo ""

echo "" $red
echo "Have you read through the script you're about to run and " $red
echo "understood that it will make changes to your computer?" $red
echo "If yes hit any key to continue, if not hit ctrl-C to quit." $red
echo "" $green
read -r response

echo "Updating and Upgrading"
	apt-get update && sudo apt-get upgrade -y

echo "Installing build essentials"
sudo apt-get install -y build-essential libssl-dev

echo "Installing Linuxbrew"
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
  test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
  test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
  test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
  echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile

echo "Installing NodeJS and NPM"
  sudo apt-get install nodejs
  sudo apt-get install npm
  nodejs -v

echo "Installing Git"
  sudo apt-get update
  sudo apt-get install git

echo "Installing Vue CLI"
  npm install -g @vue/cli

echo "Installing Arc Theme"
  sudo add-apt-repository ppa:noobslab/themes -y
  sudo apt-get update
  sudo apt-get install arc-theme -y

echo "Installing WGET and Curl"
  sudo apt-get install wget curl

echo "Installing Atchive Extractors"
  sudo apt-get install -y unace unrar zip unzip p7zip-full p7zip-rar sharutils rar uudeview mpack arj cabextract file-roller

echo "Installing F"ilezilla
  sudo apt-get install -y filezilla

echo "Installing Google Chrome"
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  sudo dpkg -i google-chrome-stable_current_amd64.deb

echo "Installing Brave"
  curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key add -
  sudo sh -c 'echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com `lsb_release -sc` main" >> /etc/apt/sources.list.d/brave.list'
  sudo apt update
  sudo apt install brave-browser brave-keyring

echo "Installing VS Code"
  curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
  sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
  sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
  sudo apt update
  sudo apt install code -y

echo "Installing VS Code Extensions"
  code --install-extension vscode-arduino
  code --install-extension octref.vetur
  code --install-extension ms-vscode.cpptools
  code --install-extension HookyQR.beautify

echo "Installing Yarn"
  brew install yarn 

echo "Installing Battery Life Saving Junk"
  sudo apt-get remove laptop-mode-tools
  sudo add-apt-repository ppa:linrunner/tlp
  sudo apt-get update
  sudo apt-get install -y tlp tlp-rdw smartmontools ethtool
  sudo tlp start
  sudo tlp stat

echo "Installing KVM Acceleration and CPU Checker"
  sudo apt-get install -y cpu-checker
  sudo apt-get install -y qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils
  sudo apt-get install -y virt-manager
  sudo apt-get install -y lib32z1 lib32ncurses5 lib32bz2-1.0 lib32stdc++6

echo "Installing Square 2.0 icons"
  sudo add-apt-repository ppa:noobslab/icons2
  sudo apt-get update
  sudo apt-get install square-icons

echo "Fixing no audio on Toshiba Chromebook 2" $cyan
    git clone https://github.com/plbossart/UCM.git 
    sudo cp -rv UCM/chtmax98090 /usr/share/alsa/ucm 
    alsactl kill quit 
    alsactl init 
    pulseaudio --kill 
    pulseaudio --start 
    
echo "Adding 3 finger swipe up/down to change workspaces" $blue
    sudo apt-get remove --purge xserver-xorg-input-synaptics
    sudo apt-get install xserver-xorg-input-libinput
    sudo gpasswd -a $USER input
    sudo apt-get install xdotool wmctrl
    sudo apt-get install libinput-tools
    git clone http://github.com/bulletmark/libinput-gestures
    cd libinput-gestures
    sudo ./libinput-gestures-setup install
    cd ..
    
echo "Installing NGROK" $yellow
    sudo npm install -g ngrok
    
echo "Installing TLP (Battery Performance)"
    sudo add-apt-repository ppa:linrunner/tlp
    sudo apt-get update
    sudo apt-get install tlp tlp-rdw
    sudo tlp start 
    
echo "Cleaning up Linux" $magenta
    sudo apt-get purge *flatpak* xdg-desktop-portal
    sudo apt-get remove fonts-kacst* fonts-khmeros* fonts-lklug-sinhala fonts-guru-extra fonts-nanum* fonts-noto-cjk fonts-takao* fonts-tibetan-machine fonts-lao fonts-sil-padauk fonts-sil-abyssinica fonts-tlwg-* fonts-lohit-* fonts-beng-extra fonts-gargi fonts-gubbi fonts-gujr-extra fonts-kalapi fonts-lohit-* fonts-samyak* fonts-navilu fonts-nakula fonts-orya-extra fonts-pagul fonts-sarai fonts-telu* fonts-wqy* fonts-smc* fonts-deva-extra fonts-sahadeva
    
echo "Change out Swappiness"
echo "Copy and paste this 'vm.swappiness=10' without single quotes then save file"
    xed admin:///etc/sysctl.conf
    
    
sudo reboot
    